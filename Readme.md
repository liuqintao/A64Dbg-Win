# A64Dbg on Windows

#### Description

An arm/arm64/x86/x64 assembly level debugger for Android on Windows.

Note: The backend lldb/python runtime only support at least Windows 10.

Follow us for update or bug report:

|Platform|Account|
|-|-|
|Email|liubaijiang@yunyoo.cn|
|公众号|江哥说安全|
|头条抖音|刘柏江/江哥说安全|
|微博|刘柏江VM|
|码云|https://gitee.com/geekneo/|
